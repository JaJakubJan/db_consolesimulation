from colorama import init, AnsiToWin32
import sys


class Color:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def add_user_to_list(name: str, surname: str, ID: int, sex: str,
                     year: int, average: float, field: str, input_list: list):
    input_list.append({
            'name':     name,
            'surname':  surname,
            'ID':       ID,
            'sex':      sex,
            'year':     year,
            'average':  average,
            'field':    field,
        })


def initialize_DB() -> 'list':
    user_list = []
    add_user_to_list('Andrzej', 'Kozłowski', 97092200003, 'M', 2, 3.14, 'Informatyka', user_list)
    add_user_to_list('Maciej', 'Guśowski', 98011200321, 'M', 2, 4.24, 'Informatyka', user_list)
    add_user_to_list('Marta', 'Brzęczywoda', 94022200123, 'K', 3, 4.48, 'Informatyka', user_list)
    add_user_to_list('Danuta', 'Igłowska', 98092203003, 'K', 4, 3.14, 'Mechatronika', user_list)
    add_user_to_list('Stefan', 'Szprycer', 99011800321, 'M', 2, 4.24, 'Informatyka', user_list)
    add_user_to_list('Franz', 'Jankowski', 94122200123, 'M', 1, 3.58, 'Mechatronika', user_list)
    add_user_to_list('Kamil', 'Kozłowski', 97032200007, 'M', 2, 3.18, 'Informatyka', user_list)
    add_user_to_list('Jakub', 'Petardowski', 99011600321, 'M', 2, 2.74, 'Matematyka', user_list)
    add_user_to_list('Karol', 'Mąciwoda', 96021230777, 'M', 1, 5.18, 'Informatyka', user_list)
    add_user_to_list('Martyna', 'Columbus', 10000000001, 'K', 4, 3.38, 'Budownictwo', user_list)
    return user_list


def show_user_by_index(user: dict):
    print(Color.BOLD + "\t" + user.get('name') + "\t" + user.get('surname') + "\t" + str(user.get('ID'))
          + "\t\t" + user.get('sex') + "\t" + str(user.get('year')) + "\t" + str(user.get('average')) + "\t\t" + user.get('field')
          + Color.ENDC, file=stream, end=',\n')


def show_all(user_list: list):
    print(Color.HEADER + "\tNAME: \tSURNAME:\tID NUMBER:\t\tSEX:\tYEAR:\tAVERAGE:\tFIELD: \t"
          + Color.ENDC, file=stream)
    for user in user_list:
        show_user_by_index(user)


def sort_by_average(user_list: list):
    user_list.sort(key=lambda x:x.get('average'))
    print(Color.OKGREEN + "\tList of users sorted by average!" + Color.ENDC, file=stream)


def sort_by_name(user_list: list):
    user_list.sort(key=lambda x:x.get('surname'))
    print(Color.OKGREEN + "\tList of users sorted by surname!" + Color.ENDC, file=stream)


def get_user(user_list: list, user_ID: int):
    user_ID = int(user_ID)
    find_user = lambda x, y: x if user_list[x].get('ID') == y else -1
    for i in range(len(user_list)):
        if find_user(i, user_ID) >= 0:
            print(Color.OKGREEN + "\tUser found!" + Color.ENDC, file=stream)
            print(Color.HEADER + "\tNAME: \tSURNAME:\tID NUMBER:\t\tSEX:\tYEAR:\tAVERAGE:\tFIELD: \t"
                  + Color.ENDC, file=stream)
            show_user_by_index(user_list[i])
            return
    print(Color.WARNING + "\tUser with ID: " + str(user_ID) + " not found!" + Color.ENDC, file=stream)


def delete_user(user_list: list, user_ID: int):
    user_ID = int(user_ID)
    find_user = lambda x, y: x if user_list[x].get('ID') == y else -1
    for i in range(len(user_list)):
        if find_user(i, user_ID) >= 0:
            print(Color.OKGREEN + "\tUser: " + Color.ENDC, file=stream)
            print(Color.HEADER + "\t\tNAME: \tSURNAME:\tID NUMBER:\t\tSEX:\tYEAR:\tAVERAGE:\tFIELD: \t\n\t"
                  + Color.ENDC, file=stream, end='')
            show_user_by_index(user_list[i])
            user_list.remove(user_list[i])
            print(Color.OKGREEN + "\tcorrectly deleted!" + Color.ENDC, file=stream)
            return
    print(Color.WARNING + "\tUser with ID: " + str(user_ID) + " not found!" + Color.ENDC, file=stream)


def separate_users_by_average(user_list: list, max_average: float):
    max_average = float(max_average)
    get_average = lambda x: user_list[x].get('average')
    sep_list = []
    for i in range(len(user_list)):
        if get_average(i) < max_average:
            sep_list.append(user_list[i])
    if len(sep_list) > 0:
        print(Color.OKGREEN + "\tUsers in DB with less average than " + str(max_average) + ":" + Color.ENDC, file=stream)
        show_all(sep_list)
    else:
        print(Color.WARNING + "\tNo users in DB with less average than " + str(max_average) + ":" + Color.ENDC, file=stream)
    return sep_list


def no_such_action(user_list: list = [], content: str = ' '):
    print(Color.FAIL + "\tWrong command!" + Color.ENDC, file=stream, end='')
    print(Color.WARNING + " Type 'help' to show command list" + Color.ENDC, file=stream)


def print_menu(user_list: list):
    print(Color.HEADER + "\tCOMMANDS: " + Color.ENDC + Color.OKBLUE + "" \
          "\n\tto show DB users:                      'show'" \
          "\n\tto quit:                               'quit'" \
          "\n\tsort users (by average):               'sort'" \
          "\n\tsort users by average:                 'sort -av'" \
          "\n\tsort users by surname:                 'sort -n'" \
          "\n\tfind and get a user:                   'get ###########' (#..# - user ID number)" \
          "\n\tdelete a user from DB:                 'del ###########' (#..# - user ID number)" \
          "\n\tget user list with av less than input: 'sep $$' ($$ - average)\n" \
          + Color.ENDC, file=stream)


def run(user_list):
    simple_actions = {"help":       print_menu,
                      "show":       show_all,
                      "sort":       sort_by_average,
                      "sort -av":   sort_by_average,
                      "sort -n":    sort_by_name}
    advanced_actions = {"get":      get_user,
                        "del":      delete_user,
                        "sep":      separate_users_by_average}
    while True:
        try:
            selection = str(input("> "))
            if "quit" == selection:
                return
            if len(selection) > 3:
                adv_command = selection[0:3:]
                if adv_command in advanced_actions.keys() and len(selection) > 4:
                    try:
                        adv_content = selection[3::]
                        advanced_actions.get(adv_command)(user_list, adv_content)
                    except ValueError:
                        print(Color.FAIL + "\tID's have to be an integer and average have to be a real number "
                              + Color.ENDC, file=stream)
                else:
                    simple_actions.get(selection, no_such_action)(user_list)
            else:
                no_such_action()
        except Exception as e:
            print(Color.FAIL + "Critical error!! " + e.data + Color.ENDC, file=stream)


if __name__ == "__main__":
    init(wrap=False)
    stream = AnsiToWin32(sys.stderr).stream
    user_list = initialize_DB()
    print(Color.OKGREEN + "DB initialization complete - enter a command:" + Color.ENDC, file=stream)
    run(user_list)
